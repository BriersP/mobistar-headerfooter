module.exports = function(grunt) {

  require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    sass: {
      options: {
        sourcemap: false,
        unixNewlines: true,
        lineNumbers: false,
        quiet: true
      },
      dist: {
        options: { style: 'compact' },
        files: { 'dist/common-header/css/mob_header_footer_revamp_min.css': 'src/sass/mob_header_footer_revamp.scss' }
      }
    },
    jshint: {
      options: {
        eqeqeq: true,
        proto: true
      },
      all: ['gruntfile.js', 'src/js/mob-ghf.js']
    },
    uglify: {
      dist: {
        files: {
          'dist/common-header/js/mob-gh.min.js': [
            'src/js/mobglobal/popup.js',
            'src/js/mobglobal/navigation.js',
            'src/js/mobglobal/helpers.js',
            'src/js/mobglobal/dom.js',
            'src/js/mobglobal/cart.js',
            'src/js/mobglobal/placeholders.js',
            'src/js/mobglobal/newsletter.js',
            'src/js/mobglobal/feedback.js',
            'src/js/mobglobal/validate.js',
            'src/js/run.js'
          ],
          'dist/common-header/js/mob-gf.min.js': [
            'src/js/search/completion.js',
            'src/js/search/searchBox.js'
          ]
        }
      }
    },
    imagemin: {
      dist: {
        files: [{
          expand: true,
          cwd: 'src/img/',
          src: ['**/*.{png,jpg,gif}'],
          dest: 'dist/common-header/img/'
        }]
      }
    },
    autoprefixer: {
      options: {
        browsers: ['last 5 version', 'ie 8', 'ie 9'], // @see: https://github.com/ai/autoprefixer#browsers
        map: true
      },
      dist: {
        src: 'dist/common-header/css/*.css'
      }
    },
    assemble: {
      options: {
        flatten: true,
        partials: ['src/templates/**/*.hbs']
      },
      nl: {
        options: {
          data: 'src/data/nl.yml'
        },
        files: {
          'dist/common-header/nl/': ['src/pages/**/*.hbs']
        }
      },
      fr: {
        options: {
          data: 'src/data/fr.yml'
        },
        files: {
          'dist/common-header/fr/': ['src/pages/**/*.hbs']
        }
      }
    },
    clean: {
      dist: ['dist'],
      zip: ['header-footer.zip']
    },
    copy: {
      dist: {
        files: [
          {
            expand: true,
            flatten: true,
            cwd: 'src/fonts/',
            src: '**',
            dest: 'dist/common-header/fonts/',
            filter: 'isFile'
          }
        ]
      }
    },
    compress: {
      dist: {
        options: {
          archive: 'header-footer.zip'
        },
        files: [
          {
            expand: true,
            cwd: 'dist/common-header/',
            src: ['**'],
            dest: 'header-footer/common-header/'
          }
        ]
      }
    },
    watch: {
      options: {
        livereload: true
      },
      styles: {
        files: ['src/sass/**/*.*'],
        tasks: ['css']
      },
      scripts: {
        files: ['gruntfile.js', 'src/js/**/*.*'],
        tasks: ['js']
      },
      templates: {
        files: ['src/templates/**/*.*', 'src/pages/**/*.*'],
        tasks: ['assemble']
      }
    }
  });
  grunt.registerTask('js', ['jshint', 'uglify']);
  grunt.registerTask('css', ['sass', 'autoprefixer']);
  grunt.registerTask('zip', ['clean:zip', 'compress:dist']);
  grunt.registerTask('default', ['clean:dist', 'copy:dist', 'css', 'js', 'assemble', 'imagemin']);
  grunt.registerTask('dropbox', ['compress:dropbox']);
};
