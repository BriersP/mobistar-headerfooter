/**
 * Trim Shim
 */
if(typeof String.prototype.trim !== 'function') {
  String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g, '');
  };
}

/**
 * RUNS ON DOM LOAD
 */
mobGlobal.dom.isReady(function() {
  with(mobGlobal) {

    var helpers = helpers();

    // Newsletter
    var newsletter = newsletter();
    newsletter.init();

    // Dynamic Cart items
    var cart = cart();
    cart.init();

    // Satisfaction popup
    var popup = popup();
    popup.init();

    // Faux Placeholders
    var placeholders = placeholders();
    placeholders.init();

    // Header navigation stuff
    var navigation = navigation();
    navigation.init();

//    completionMosse.enable();

    // Show extra functionality for logged in users
    if(helpers.isUserLoggedOn()) {
      var loggedOffOnlyItems = document.querySelectorAll('.on-logged-off');
      for(var i = 0; i < loggedOffOnlyItems.length; ++i) {
        loggedOffOnlyItems[i].style.display = "none";
      }
    }

    // NOTIFICATION
    // @see: demo-notification.html for json structure.
    helpers.ajaxGet('/sites/b2c/files/notification.json',
      function(data) {
        if(data) {
          var data = JSON.parse(data);
          var message = data.message[mobGlobal.lang.toLowerCase()];
          var elem = document.getElementById('mob-global-notification');
          if(message != undefined && message != "") {
            elem.innerHTML = message;
            elem.className = "level-" + data.level;
            elem.style.display = "block";
          }
        }

      }
    );

    // Sets the correct URIs on the languageswitcher
    // B2C website gives a Drupal object with language_switcher options
    var uriLang = (typeof Drupal !== "undefined" && Drupal.settings && Drupal.settings.language_switcher) ?
      Drupal.settings.language_switcher :
      { nl: '/nl', fr: '/fr' };
    document.querySelector('.nav.lang-sel .lang-nl').href = uriLang.nl;
    document.querySelector('.nav.lang-sel .lang-fr').href = uriLang.fr;

  }
});
