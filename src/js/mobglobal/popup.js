if(mobGlobal === undefined) {
  var mobGlobal = {};
}

mobGlobal.popup = (function () {

  var parent = this;

  return {
    elems: {
      backdrop: document.getElementById('mob-global-popup-backdrop'),
      popup: document.getElementById('mob-global-popup'),
      button: {
        ok: document.getElementById('footer-feedback-ok'),
        nok: document.getElementById('footer-feedback-nok')
      },
      close: document.querySelectorAll('.close_window')
    },
    /**
     *
     */
    init: function() {
//      parent.dom.addEventHandler(this.elems.button.ok, 'click', this.ajax);
      parent.dom.addEventHandler(this.elems.button.ok, 'click', this.iframe);
      parent.dom.addEventHandler(this.elems.button.ok, 'click', this.show);
//      parent.dom.addEventHandler(this.elems.button.nok, 'click', this.ajax);
      parent.dom.addEventHandler(this.elems.button.nok, 'click', this.iframe);
      parent.dom.addEventHandler(this.elems.button.nok, 'click', this.show);
      parent.dom.addEventHandler(this.elems.backdrop, 'click', this.hide);
      parent.dom.addEventHandler(this.elems.backdrop, 'click', this.hide);
    },

    /**
     * Hide the satisfaction popup
     * @param test
     * @returns {*}
     */
    hide: function(evt) {
      parent.popup.elems.backdrop.style.display = "none";
      parent.popup.elems.popup.style.display = "none";
    },
    /**
     * Show the satisfaction popup
     * @param evt
     */
    show: function(evt) {
      parent.popup.elems.popup.style.display = "block";
      parent.popup.elems.backdrop.style.display = "block";
      // Cancel submit
      if (evt.preventDefault) {
        evt.preventDefault();
      } else if (window.event) { // for IE
        window.event.returnValue = false;
      }
    },
    ajax: function(evt) {
      // Cancel submit
      if (evt.preventDefault) {
        evt.preventDefault();
      } else if (window.event) { // for IE
        window.event.returnValue = false;
      }
      parent.helpers.ajaxGet(evt.target.href, function(data) {
        var data = JSON.parse(data);
        document.getElementById('mob-global-popup').innerHTML = data[1].data;
      });
    },
    /**
     * Shows an iframe in the popup, with the uri of the link clicked
     */
    iframe: function(evt) {
      var url = evt.target.href;
      var popup = document.getElementById('mob-global-popup');
      while (popup.firstChild) {
        popup.removeChild(popup.firstChild);
      }
      var iframe = document.createElement('iframe');
      iframe.src = url; // iframe.src = iframe.src;
      popup.appendChild(iframe); //innerHTML = '<iframe src="http://peterbriers.be"></iframe>';
    }
  }
});
