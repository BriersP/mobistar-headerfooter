if(mobGlobal === undefined) {
  var mobGlobal = {};
};

mobGlobal.cart = (function () {

  var parent = this;

  return {
    elems: {
      items: document.getElementById('shoppingcart-items'),
      actions: document.getElementById('shoppingcart-actions')
    },
    cookie: parent.helpers.getCookie('mobicart'), // Gets the amount of car items
    init: function() {
      if(this.cookie) {
        this.elems.items.innerHTML = this.cookie;
      }
      if(isNaN(this.elems.items.innerHTML)) {
        this.elems.actions.querySelector('.fade').style.display = 'block';
      }
    }
  };

});
