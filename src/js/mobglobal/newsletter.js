if(mobGlobal === undefined) {
  var mobGlobal = {};
};

mobGlobal.newsletter = (function () {

  var parent = this;

  return {
    elems: {
      form: document.querySelector('#mob-global-form-newsletter'),
      input: document.querySelector('#mob-global-form-newsletter input'),
      error: document.querySelector('#mob-global-form-newsletter .error-msg')
    },
    init: function() {
      parent.dom.addEventHandler(this.elems.form, 'submit', this.submit);
      parent.dom.addEventHandler(this.elems.error, 'click', this.hideError);
    },
    submit: function(e) {
      var form = e.target;
      var input = form.querySelector('input');
      var email = input.value;
      var error = form.querySelector('.error-msg')
      // Cancel submit
      if (e.preventDefault) {
        e.preventDefault();
      } else if (window.event) { // for IE
        window.event.returnValue = false;
      }
      // Validate e-mail
      if( ! parent.validate.email(email)) {
        error.style.display = 'block';
        return false;
      }
      form.action = "http://www.mobistar.be/fr/newsletters/?mail="+email+"&mail2="+email;
      form.submit();
    },
    hideError: function(e) {
      e.target.style.display = 'none';
    }
  };

});
