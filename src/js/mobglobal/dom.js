if(mobGlobal === undefined) {
  var mobGlobal = {};
}

/**
 * Shims are good, but we won't use them because they can break functionalities of existing libraries like jQuery.
 */
mobGlobal.dom = {

  /**
   * Helper functior Event Handling
   * Shims & Polyfills may break jQuery stuff...
   *
   * @param elem
   * @param eventType
   * @param handler
   */
  addEventHandler: function(elem, eventType, handler) {
    if (elem.addEventListener)
      elem.addEventListener (eventType,handler,false);
    else if (elem.attachEvent)
      elem.attachEvent ('on'+eventType,handler);
  },

  /**
   * On Document Ready
   * @param {function} The function that needs to be executed after DOM load
   */
  isReady: function(fn) {
    if (document.addEventListener) {
      document.addEventListener('DOMContentLoaded', fn);
    } else {
      document.attachEvent('onreadystatechange', function() {
        if (document.readyState === 'interactive')
          fn();
      });
    }
  },

  /**
   * Checks if an element has a class
   * @param el - The element is should match against
   * @param {string} matchClass - The class that it should match
   * @returns {boolean}
   */
  hasClass: function(el, matchClass) {
    return (" " + el.className + " ").indexOf(" " + matchClass + " ") > -1;
  }
}

