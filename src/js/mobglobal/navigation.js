if(mobGlobal === undefined) {
  var mobGlobal = {};
}

mobGlobal.navigation = (function () {

  var parent = this;

  return {
    elems: {
      items: [],
      backdrop: document.getElementById('mob-global-backdrop')
    },
    init: function() {
      this.elems.items = this.getItems(['nav-item-ps', 'nav-item-store', 'nav-item-blog', 'nav-item-hs', 'nav-item-cz']);
      for (var i = 0; i < this.elems.items.length; ++i) {
        var item = this.elems.items[i];
        parent.dom.addEventHandler(item.item, 'mouseenter', this.hover(item));
        parent.dom.addEventHandler(item.item, 'mouseleave', this.reset());
      }
    },
    /**
     * Hover effects for the main navigation
     * Uses special parameter injection wrapper
     * @param {object} the navItem object that is hovered
     * @returns {Function}
     */
    hover: function(item) {
      var backdrop = this.elems.backdrop;
      return function() {
        item.link.className += " hover";
        if(parent.helpers.isHomepage()) { return false; }
        if(parent.helpers.isUserLoggedOn() && parent.dom.hasClass(item.overlay, 'on-logged-off')) { return false; }
        item.overlay.style.display = "block";
        backdrop.style.display = "block";
      };
    },
    /**
     * Reset the menu hover-stuff
     */
    reset: function() {
      var self = this;
      return function() {
        var items = self.elems.items;
        var backdrop = self.elems.backdrop;
        for(var i = 0; i < items.length; i++) {
          items[i].link.className = items[i].link.className.replace(" hover", "");
          if( ! parent.helpers.isHomepage()) {
            items[i].overlay.style.display = "none";
            backdrop.style.display = "none";
          }
        }
      }
    },
    /**
     * Build up an object through a given array
     * @param {array} navIDs - An array of the IDs of the navigation items
     * @return {object}
     */
    getItems: function(navIDs) {
      var items = [];
      for (i = 0; i < navIDs.length; ++i) {
        var el = document.getElementById(navIDs[i]);
        if(el !== null) {
          var current = document.getElementById(navIDs[i]);
          var item = {
            'item': current,
            'link': current.querySelector('a'),
            'overlay': current.querySelector('.overlay')
          };
          items.push(item);
        }
      }
      return items;
    }
  }
});
