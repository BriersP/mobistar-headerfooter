if(mobGlobal === undefined) {
  var mobGlobal = {};
}

mobGlobal.validate = {

  /**
   * Checks if a string is a correct formatted e-mail
   * @param {string} email - E-mail to check
   * @returns {boolean}
   */
  email: function(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }
}
