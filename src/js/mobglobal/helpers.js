if(mobGlobal === undefined) {
  var mobGlobal = {};
}

mobGlobal.helpers = (function () {

  var parent = this;

  return {
    /**
     * Is this the homepage?
     * @returns {boolean}
     */
    isHomepage: function() {
      // console.log('location.pathname: ' + location.pathname);
      return (location.pathname === '/');
    },

    /**
     * Checks if the user is logged on
     * @returns {boolean}
     */
    isUserLoggedOn: function() {
      var session = parent.helpers.getCookie('SMSESSION');
//      console.log('session: ' + session);
//      console.log('typof session: ' + typeof session);
      return (typeof session !== 'undefined' && session !== 'LOGGEDOFF');
    },

    /**
     * Get the value of a cookie with name
     * @param {string} cname - The name of the cookie
     * @return {string} {undefined}
     */
    getCookie: function(cname) {
      var name = cname + "=";
      var ca = document.cookie.split(';');
      for(var i=0; i<ca.length; i++) {
        var c = ca[i].trim();
        if (c.indexOf(name) === 0) return c.substring(name.length,c.length);
      }
      return undefined;
    },
    /**
     * Execute an ajax request
     * @param url
     * @param callback on success
     * @param callback on error
     */
    ajaxGet: function(url, callbackSuccess, callbackError) {
      var request = new XMLHttpRequest();
      request.open('GET', url, true);
      request.onreadystatechange = function() {
        if (this.readyState === 4){
          if (this.status >= 200 && this.status < 400) {
            callbackSuccess(this.responseText);
          } else {
            if(typeof callbackError == "function") {
              callbackError();
            }
          }
        }
      };
      request.send();
      request = null;
    }
  }
});
