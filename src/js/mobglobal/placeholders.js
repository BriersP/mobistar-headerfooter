if(mobGlobal === undefined) {
  var mobGlobal = {};
};

mobGlobal.placeholders = (function () {

  var parent = this;

  return {
    elems: document.querySelectorAll('#mob-global-form-login input, #mob-global-form-newsletter input'),
    placeholders: [],
    init: function() {
      for(var i = 0; i < this.elems.length; ++i) {
        this.placeholders[i] = this.elems[i].value;
        parent.dom.addEventHandler(this.elems[i] ,'focus', this.hide(this.elems[i], i));
        parent.dom.addEventHandler(this.elems[i] ,'blur', this.show(this.elems[i], i));
      }
    },
    hide: function(el, i) {
      var self = this;
      return function() {
        if(el.value == self.placeholders[i]) {
          el.value = ""
        }
      }
    },
    show: function(el, i) {
      var self = this;
      return function() {
        if(el.value == "") {
          el.value = self.placeholders[i];
        }
      }
    }
  };

});
