function changeCompletion (langue, input){
    propertiesMosse.additionalParams="pays=obe&langue=" + langue + "&type=res"
    propertiesMosse.blocks[0].label = blocksTitle[langue]['Store'];
    propertiesMosse.blocks[1].label = blocksTitle[langue]['Assistance'];
    propertiesMosse.blocks[2].label = blocksTitle[langue]['Results'];
    completionMosse.restart(propertiesMosse);
    setTimeout(function() { input.focus(); }, 10);
    document.getElementById('langue').value = langue;
    input.placeholder = blocksTitle[langue]['Placeholder'];
}

function addEvent( obj, type, fn )
{
    if (obj.addEventListener) {
        obj.addEventListener( type, fn, false );
        EventCache.add(obj, type, fn);
    }
    else if (obj.attachEvent) {
        obj["e"+type+fn] = fn;
        obj[type+fn] = function() { obj["e"+type+fn]( window.event ); }
        obj.attachEvent( "on"+type, obj[type+fn] );
        EventCache.add(obj, type, fn);
    }
    else {
        obj["on"+type] = obj["e"+type+fn];
    }
}

var EventCache = function()
{
    var listEvents = [];

    return {
        listEvents : listEvents,

        add : function(node, sEventName, fHandler){
            listEvents.push(arguments);
        },

        flush : function(){
            var i, item;
            for (i = listEvents.length - 1; i >= 0; i = i - 1){
                item = listEvents[i];

                if(item[0].removeEventListener){
                    item[0].removeEventListener(item[1], item[2], item[3]);
                };

                if(item[1].substring(0, 2) != "on"){
                    item[1] = "on" + item[1];
                };

                if(item[0].detachEvent){
                    item[0].detachEvent(item[1], item[2]);
                };

                item[0][item[1]] = null;
            };
        }
    };
}();

addEvent(window,'unload',EventCache.flush);

// function use to create the HTML
var Flux = new Array();
var isIE = (document.all) ? true : false;
Array.prototype.ArrayPos = function(quoi)
{
    var Position = -1;
    for (i = 0; this[i]; i++)
    {
        if (quoi == this[i])
        {
            Position = i;
        }
    }
    return Position;
}

$Close=function(Closing)
{
    var FluxPos = Flux.ArrayPos(Closing)
    var FluxLength = Flux.length;
    var SpliceLength = FluxLength - FluxPos + 1;
    var AppendLot = Flux.splice(FluxPos + 1, SpliceLength);

    for(l = 0; AppendLot[0]; l++)
    {
        Closing.appendChild(AppendLot.shift());
    }
    if(Flux.length == 1)
    {
        var arrCartouches = document.querySelectorAll('.cartouche');
        var flux_form = Flux.pop();
        for(i = 0; i < arrCartouches.length; ++i) {
          var flux_form_clone = flux_form.cloneNode(true);
          arrCartouches[i].appendChild(flux_form_clone);
        }
    }
}

var $Component = function (htmlObj)
{

    var Elt = '';

    Elt = htmlObj['obj'];

    if(htmlObj['obj'] == 'form')
    {
        Elt = (isIE) ? ('<form name="'+htmlObj['name']+' >') : htmlObj['obj'];
    } else
    {
        if (('name' in htmlObj) && (htmlObj['obj'] == 'input') && (isIE))
        {
            Elt = '<input name="' + htmlObj['name']+'"';
            if ('value' in htmlObj)
            {
                Elt += ' value="'+htmlObj['value']+'"';
            }
            Elt += ' >';
        }
    }

    var Obj = document.createElement(Elt);
    for (elt in htmlObj)
    {
        if (elt != 'obj')
        {
            if(elt == 'css')
            {
                for(Styl in htmlObj['css'])
                {
                    if(Styl == 'cssFloat' && isIE)
                    {
                        Obj.style['float'] = htmlObj['css'][Styl];
                    } else
                    {
                        Obj.style[Styl] = htmlObj['css'][Styl];
                    }
                }
            }else
            {
                if(isIE)
                {
                    if(!isIE || (elt != 'name' && elt != 'for' ))
                    {
                        Obj[elt] = htmlObj[elt];
                    }
                } else
                {
                    Obj[elt] = htmlObj[elt];
                }
            }
        }
    }
    Flux.push(Obj);
    return Obj;
 }

 function closeSearch()
 {
    document.getElementById('searchForm').reset();
    document.getElementById('closeSearch').style.visibility = 'hidden';
 }

 function getBHV()
 {
    var selectedBlockId = completionMosse.getAllSuggestionParams(completionMosse.getSuggestionPrefix()+ completionMosse.getHighlightedSuggestionIndex());
    if (typeof selectedBlockId !== 'undefined' && selectedBlockId !==null)
        document.getElementById('bhv').value = propertiesMosse.blocks[selectedBlockId['blockId']]['bhv'];
 }

function submit()
{
    getBHV();
    document.searchForm.submit();
}

// Initialize
var init = function createCartouche()
{
    this.bhvSelect                          = 'results';
//    if (typeof Drupal.settings.search !== 'undefined') {
//        if (typeof Drupal.settings.search.bhv !== 'undefined') {
//            this.bhvSelect                  = Drupal.settings.search.bhv;
//        }
//    }
    this.searchurl                          = 'http://search.mobistar.be';
    this.searchcmplurl                      = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'completion.ke.voila.fr';
    this.pays                               = 'obe';
    this.lg                                 = mobGlobal.lang.toLowerCase();
    this.type                               = 'res';

    this.blocksTitle                        = new Array();
    this.blocksTitle['fr']                  = new Array();
    this.blocksTitle['fr']['Store']         = '> Magasin';
    this.blocksTitle['fr']['Assistance']    = '> Aide';
    this.blocksTitle['fr']['Results']       = '> Résultats';
    this.blocksTitle['fr']['For um']         = '> Forum';
    this.blocksTitle['fr']['Placeholder']   = 'Rechercher';

    this.blocksTitle['nl']                  = new Array();
    this.blocksTitle['nl']['Store']         = '> Winkel';
    this.blocksTitle['nl']['Assistance']    = '> Hulp';
    this.blocksTitle['nl']['Results']       = '> Resultaten';
    this.blocksTitle['nl']['Forum']         = '> Forum';
    this.blocksTitle['nl']['Placeholder']   = 'Zoeken';

    if(isIE)
    {
        var mycenterdiv = $Component({
                                        'obj'   :'div',
                                        'css'   :{
                                                    'border'    :'1px solid #C5C5C5',
                                                    'height'    : '35px',
                                                    'display'   : 'inline-block',
                                                    'margin'    : '0px 0px 0px',
                                                    'position'  : 'relative',
                                                    'textAlign' : 'left',
                                                    'width'     : '453px'
                                                }
                                    });

    }

    var myform          = $Component({
                                        'obj'           : 'form',
                                        'id'            : 'searchForm',
                                        'name'          : 'searchForm',
                                        'method'        : 'get',
                                        'action'        : this.searchurl,
                                        'css'           : {
                                                                'textAlign':'left'
                                                          },
                                        'autocomplete'  : 'off'
                            });

    var myinputmodule   = $Component({
                                        'obj'       : 'input',
                                        'id'        : 'module',
                                        'name'      : 'module',
                                        'value'     : 'emea',
                                        'type'      : 'hidden'
                                    });

    var myinputbhv      = $Component({
                                        'obj'       : 'input',
                                        'id'        : 'bhv',
                                        'name'      : 'bhv',
                                        'value'     : this.bhvSelect,
                                        'type'      : 'hidden'
                                    });

    var myinputpays     = $Component({
                                        'obj'       : 'input',
                                        'id'        : 'pays',
                                        'name'      : 'pays',
                                        'value'     : this.pays,
                                        'type'      : 'hidden'
                                    });

    var myinputlg       = $Component({
                                        'obj'       : 'input',
                                        'id'        : 'langue',
                                        'name'      : 'langue',
                                        'value'     : this.lg,
                                        'type'      : 'hidden'
                                    });

    var myinputtype     = $Component({
                                        'obj'       : 'input',
                                        'id'        : 'type',
                                        'name'      : 'type',
                                        'value'     : this.type,
                                        'type'      : 'hidden'
                                    });

    if(!isIE)
    {
        var myinput = $Component({
                                    'obj'           : 'input',
                                    'placeholder'   : this.blocksTitle[this.lg]['Placeholder'],
                                    'id'            : 'searchInputId',
                                    'type'          : 'text',
                                    'name'          : 'kw'
                                });

        var mybuttonClosesearch = $Component({
                                                'obj'       : 'a',
                                                'id'        : 'closeSearch',
                                                'href'      : 'javascript:closeSearch()',
                                                'css'       : {
                                                                    'backgroundImage'   : 'url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAMAAABhEH5lAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAALRQTFRFAAAAzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzc3Nzs7Oz8/P0NDQ09PT1NTU19fX2dnZ39/f5OTk5ubm5+fn6Ojo6urq6+vr7Ozs9fX19vb2+vr6/f39////P/ZWoAAAACZ0Uk5TAAMJDxgbIScqLTlIS1FUV1ppdXh7foGTn6K9wMbJ2+Tq7fD2+fy7gc77AAAApUlEQVQYGY3B2VLCQBRF0cOkgmCYo4IGEDfQDA5oOnD//79sUpA3q1hLut7t4wSSuKqLUn9ObtbW2ROFnnItYOuAjwXzuoLyBHZH7/i1FF4UNIEvsyw18yu4kzQg+LbAOyCSFHOSmR3XBB1JA4LUgswBD5Lugb2Z/zHLlnAjqZTA5uAde/uEkU4iYO2AHbw3lIspdHTWfSM3jVSoDcfJ63O3on/8AaK8HV0WX4yZAAAAAElFTkSuQmCC")',
                                                                    'backgroundRepeat'  : 'no-repeat',
                                                                    'display'           : 'none',
                                                                    'position'          : 'absolute',
                                                                    'left'              : '365px',
                                                                    'top'               : '17px',
                                                                    'height'            : '35px',
                                                                    'width'             : '18px'
                                                               }
                                             });

        $Close(mybuttonClosesearch);

        var bgImage;
        var nav = (navigator.userAgent);
        if (nav.indexOf('Chrome') !== -1  || nav.indexOf('Safari') !== -1)
        {
            bgImage = '-webkit-gradient(linear, left top, left bottom, from(#FFFFFF), to(#BBBBBB))';
        } else if (nav.indexOf('Opera') !== -1 )
        {
            bgImage = '-o-linear-gradient(#BBBBBB 0%, #FFFFFF 100%)';
        } else
        {
            bgImage = '-moz-linear-gradient(center bottom , #BBBBBB 0px, #FFFFFF 100%)';
        }

    }else
    {

        var myinput =   $Component({
                                        'obj'           : 'input',
                                        'placeholder'   : this.blocksTitle[this.lg]['Placeholder'],
                                        'id'            : 'searchInputId',
                                        'type'          : 'text',
                                        'name'          : 'kw',
                                        'css'           : {
                                                                'color'             :'#000000',
                                                                'border'            : 'medium none',
                                                                'display'           : 'inline-block',
                                                                'height'            : '25px',
                                                                'margin'            : '0px 25px 0px 0px',
                                                                'outline'           : 'medium none',
                                                                'fontSize'          : '17px',
                                                                'padding'           :'7px 0 0 10px',
                                                                'width'             : '370px',
                                                                'verticalAlign'     : 'top'
                                                             }
                                    });

        var mybuttonClosesearch     =   $Component({
                                                        'obj'       :'a',
                                                        'id'        :'closeSearch',
                                                        'href'      :'javascript:closeSearch()',
                                                        'css'       : {
                                                                            'backgroundImage'       : 'url("./close_search.png")',
                                                                            'backgroundRepeat'      : 'no-repeat',
                                                                            'display'               : 'none',
                                                                            'position'              : 'absolute',
                                                                            'left'                  : '380px',
                                                                            'top'                   : '8px',
                                                                            'height'                : '33px',
                                                                            'width'                 : '18px'
                                                                        }
                                                    });

        $Close(mybuttonClosesearch);
    }


    $Close(myform);

    if(isIE)
        $Close(mycenterdiv);

}();

(function ()
{
    // Add event on the input and cancel button
    var cancel  = document.getElementById('closeSearch');
    var input   = document.getElementById('searchInputId');


    var checkInputClearButton = function() {
        if (input.value)
            cancel.style.display = 'block';
        else
            cancel.style.display = 'none';
    }

    addEvent(input,"blur", function () {
        cancel.style.display = 'none';
    });

    addEvent(input, "keyup",  checkInputClearButton);
    addEvent(input, "change", checkInputClearButton);
    addEvent(input, "focus",  checkInputClearButton);

    addEvent(cancel, "mousedown", function (e) {
        e = e || window.event;
        e.preventDefault ? e.preventDefault() : e.returnValue = false;
        input.value = '';
        cancel.style.display = 'none';

        // Simulate a normal key entry to update the completer after cancelling search input
        completionMosse.defaultFieldOnKeyUpCallBack({keyCode:-1});

        setTimeout(function() { input.focus(); }, 10);
    });

})();

var propertiesMosse =
{
    'url'                       : this.searchcmplurl + '/proxy/cmplGsa',
    'blocks'                    :   [
                                        {
                                            'label'    : this.blocksTitle[this.lg]['Store'],
                                            'bhv'      : 'store',
                                            'max'      : 3
                                        },
                                        {
                                            'label'    : this.blocksTitle[this.lg]['Assistance'],
                                            'bhv'      : 'assistance',
                                            'max'      : 3
                                        },
                                        {
                                            'label'    : this.blocksTitle[this.lg]['Results'],
                                            'bhv'      : 'results',
                                            'max'      : 3
                                        },
					{
                                            'label'    : this.blocksTitle[this.lg]['Forum'],
                                            'bhv'      : 'forum',
                                            'max'      : 3
                                        }
                                    ],
    'divCompletionId'           : 'mydivcmplsuggest',
    'field'                     : document.getElementById('searchInputId'),
    'queryParamName'            : 'kw',
    'additionalParams'          : 'pays=' + encodeURIComponent(this.pays) + '&langue=' + encodeURIComponent(this.lg) + '&type=' + encodeURIComponent(this.type),
    'cssPrefix'                 : 'cmpl',
    'maxNbSuggestions'          : 10,
    'maxNbChar'                 : 45,
    'clearAccents'              : false,
    'callbacks'                 :   {
                                        'submit' : {'method' : 'submit'}
                                    },
    'closeCompletionOnBlur'     : true,
    'closeLink'                 : false,
    'nameOfInstanceForJsonP'    : 'completionMosse',
    'plugins'                   : ['DirectLink']
};

var completionMosse = new orangesearch.completion.Component(propertiesMosse);
completionMosse.start();
